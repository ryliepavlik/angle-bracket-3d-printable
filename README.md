# angle-bracket-3d-printable

3D printable 90 degree right angle bracket for home repairs. See <https://www.prusaprinters.org/prints/28063-90-degree-right-angle-bracket-for-household-repair>

CC-BY-4.0 https://creativecommons.org/licenses/by/4.0/